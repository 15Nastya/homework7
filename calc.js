'use strict'
alert('Калькулятор');

let firstNumber = prompt('Введите первое число');

if(firstNumber === '') {
    alert('Первое число не указано');
} 

let operand = prompt('Введите операцию');

if(operand !== '+' && operand !== '-' && operand !=='*' && operand !=='/') {
    alert('Программа не поддерживает такую операцию');
}

let secondNumber = prompt('Введите второе число');

if(secondNumber === ''){
    alert('Второе число не указано');
}

if (isNaN(+firstNumber) || isNaN (+secondNumber)) {
    alert ('Некорректный ввод чисел');
} else {
    if(operand ==='+') console.log (+firstNumber + +secondNumber);
    if(operand ==='-') console.log (+firstNumber - +secondNumber);
    if(operand ==='*') console.log (+firstNumber * +secondNumber);
    if(operand ==='/') console.log (+firstNumber / +secondNumber);
}


